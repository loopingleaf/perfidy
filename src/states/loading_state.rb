# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

class LoadingState < Farewell::State
  include Singleton

  def initialize
    super
    @waiting_message = Farewell::Entity.new(Text.new(t(:t_waiting, scope: :loading)), 0, 64, 2, h_align: :center)
  end

  def enter
    super
    Farewell::Playlist.instance.clear
    Farewell::Playlist.instance.add(Gosu::Song.new("data/music/The_dance_of_death_part2.wav"), false)
    Farewell::Playlist.instance.add(Gosu::Song.new("data/music/The_dance_of_death_loop2_2.wav"), true)
    Farewell::Playlist.instance.play
    @players_count = Farewell::Entity.new(Text.new(Session.instance.salon.player_count.to_s), -48, 86, 2, h_align: :center)
    @min_to_begin = Farewell::Entity.new(Text.new("/6 " + t(:players, scope: :global)), 7, 86, 2, h_align: :center)
    @back_btn = Farewell::Entity.new(Text.new(t(:back, scope: :global)), 0, 128, 2, h_align: :center)
    @back_btn.add_event(@@base_events[:button])
    back_click = Farewell::Event.new
    back_click.on_click do
      Farewell::State.switch(HomeState.instance)
      Session.instance.user.leave_salon
    end
    @back_btn.add_event(back_click)
  end

  def leave
    super
  end

  def needs_redraw?
    super
  end

  def update
    super
    status = Global::CLIENT.get_pending("update")
    return if status.nil? || status["salon"].nil?

    Session.instance.salon = Salon.new(status["salon"]["player_count"])
    @players_count.drawable = Text.new(Session.instance.salon.player_count.to_s)
    return unless status["status"] == "preparing"

    Session.instance.character = Character.list[status["character_id"] - 1]
    # TODO: Send confirmation to the server to tell that we are joining
    Farewell::State.switch(PreparationState.instance)
  end

  def draw
    @waiting_message.draw
    @players_count.draw
    @min_to_begin.draw
    @back_btn.draw
    super
  end

  def button_down(id)
    super
  end
end
