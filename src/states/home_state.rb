# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

class HomeState < Farewell::State
  include Singleton

  def initialize
    super
  end

  def enter
    super
    @quick_menu = Farewell::Entity.new(
      Text.new(t(:b_quick, scope: :home)), 36, 32, 2, hitbox: 6
    )
    @party_menu = Farewell::Entity.new(
      Text.new(t(:b_create, scope: :home)), 36, 64, 2, hitbox: 6
    )
    @settings_menu = Farewell::Entity.new(
      Text.new(t(:b_settings, scope: :home)), 36, 98, 2, hitbox: 6
    )
    @entity_list.each { |entity| entity.add_event(@@base_events[:button]) }
    # @arrow = Farewell::Entity.new(Farewell::Drawable.new(Window.instance.tileset_list[:font1][47]), 0, 0, 2)
    # @draw_arrow = false
  end

  def leave
    super
  end

  def needs_redraw?
    super
  end

  def update
    super
    status = Global::CLIENT.get_pending("salon_join")
    unless status.nil?
      if status["status"] == "success"
        Session.instance.salon = Salon.new(status["salon"]["player_count"])
        Farewell::State.switch(LoadingState.instance)
      end
    end
  end

  def draw
    @quick_menu.draw
    @party_menu.draw
    @settings_menu.draw
    @arrow.draw if @draw_arrow
    super
  end

  def button_up(id)
    super
    Session.instance.user.join_salon if @quick_menu.clicked?
  end
end
