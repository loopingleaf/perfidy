# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

class SettingsState < Farewell::State
  include Singleton

  def initialize
    super
    @settings_text = GameObject.new(Text.create('Settings', 1, 0, 2))
    @fullscreen_menu = GameObject.new(Text.create('Fullscreen / Windowed',
                                                  2, 4, 2),
                                      tag: :fullscreen, hitbox: 6,
                                      highlight_tile: Text.create('>', 1, 4, 2))
    @back_menu = GameObject.new(Text.create('Back', 2, 6, 2),
                                tag: :back,
                                hitbox: 6,
                                highlight_tile: Text.create('>', 1, 6, 2))
    @quit_menu = GameObject.new(Text.create('Quit to Menu', 2, 8, 2),
                                tag: :quit, hitbox: 6,
                                highlight_tile: Text.create('>', 1, 8, 2))
  end

  def enter
    super
  end

  def leave
    super
  end

  def needs_redraw?
    super
  end

  def update
    super
  end

  def draw
    super
    @settings_text.draw
    @fullscreen_menu.draw
    @back_menu.draw
    @quit_menu.draw if Window.instance.playing
  end

  def button_down(id)
    case id
    when Gosu::KB_ESCAPE
      if Window.instance.playing
        Farewell::State.switch(PlayState.instance)
      else
        Farewell::State.switch(StartState.instance)
      end
    when Gosu::MS_LEFT
      clicked = GameObject.find(Window.instance.mouse_x.to_i, Window.instance.mouse_y.to_i)
      if clicked
        case clicked[0].tag
        when :fullscreen
          Window.instance.fullscreen = 1
        when :back
          if Window.instance.playing
            Farewell::State.switch(PlayState.instance)
          else
            Farewell::State.switch(StartState.instance)
          end
        when :quit
          Farewell::State.switch(StartState.instance)
        end
      end
    end
  end
end
