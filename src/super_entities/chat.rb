# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# @todo Manage selection
class Chat
  attr_accessor :chatting, :dialog_box, :history

  # Creates a chat entity
  #
  # @param dialog_box [Farewell::Entity] The input box where the text
  #   is inputed by the user, to be sent in chat.
  # @param history [Farewell::Entity] The history of messages.
  # @param cursor [Gosu::Image] The blinking cursor image.
  # @param font_size [Integer] The height of the font.
  # @param direction [:upward, :downward] The direction of the history.
  # @param offset [Integer]
  #
  # @see Farewell::Entity#initialize
  def initialize(dialog_box, history, cursor, font_size, direction = :upward,
    offset: 18)
    @history_text = ""
    @dialog_box = dialog_box
    @history = history
    @offset = offset
    @cursor = Farewell::Entity.new(cursor, dialog_box.x + offset,
      dialog_box.y + offset)
    @font_size = font_size
    @direction = direction
    @font = Gosu::Font.new(@font_size)
    @chatting = false
    chat_event = Farewell::Event.new
    chat_event.on_click do
      unless @chatting
        @chatting = true
        Window.instance.text_input = Gosu::TextInput.new
      end
    end
    @dialog_box.add_event(chat_event)
    Window.instance.text_input = nil
  end

  def update
    recieve
  end

  def draw
    @dialog_box.draw
    @history.draw
    return unless @chatting

    font_width = @font.text_width(Window.instance.text_input.text)
    font_x =
      if font_width >= @dialog_box.width - (@offset * 2)
        @dialog_box.x - (font_width - @dialog_box.width + @offset)
      else
        @dialog_box.x + @offset
      end
    text_till_cursor =
      Window.instance.text_input.text[0, Window.instance.text_input.caret_pos]
    @cursor.x = font_x + @font.text_width(text_till_cursor)
    @font.draw_text(Window.instance.text_input.text, font_x,
      @dialog_box.y + @offset, 4)
    @cursor.draw
  end

  # Checks for a command at the beginning of the string and sends the message to
  # the server. If the command (+/say+, +/yell+, etc.) is mispelled, the message
  # is sent anyway with the falsy message +type+ and it's to the server to
  # handle the issue.
  def send
    unless Window.instance.text_input.text.nil? ||
        Window.instance.text_input.text == ""
      request = {chat: {token: Session.instance.user.token}}
      msg = Window.instance.text_input.text.gsub(/\"/, '\"')
      command = msg.match(/^\/([0-9a-z]+)/)
      type = "say"
      unless command.nil?
        msg[/^\/([0-9a-z]+)\s?/] = "" # We remove the command and one space after
        type = command[1]
        if type == "whisper" || type == "w"
          recipients_regex = /^\s*@\w*(\s+@\w*)*/
          dirty_match = msg.match(recipients_regex)
          msg[recipients_regex] = ""
          msg[/^\s?/] = ""
          unless dirty_match.nil? || msg == ""
            recipients_match = dirty_match[0].match(/@(\w*)\s+/) # Creates the array of recipients
            recipients = recipients_match.to_a
            recipients.shift
            recipients << Session.instance.character.nickname
            characters = Character.get_by_nicks(recipients).compact
            recipient_ids = []
            characters.each { |c| recipient_ids << c.id }
            request[:chat][:recipients] = recipient_ids
          end
        end
      end
      request[:chat][:type] = type
      request[:chat][:message] = msg
      Global::CLIENT.send(request.to_json)
    end
    @chatting = false
    Window.instance.text_input = nil
  end

  def recieve
    chat_message = Global::CLIENT.get_pending("chat")
    return if chat_message.nil?

    from = chat_message["from"] || "Undefined"
    if chat_message["from"].class == Integer
      from = Character.get_by_id(chat_message["from"]).nickname
    end
    case chat_message["type"]
    when "say"
      msg_markup_begin = ""
      msg_markup_end = ""
    when "yell"
      msg_markup_begin = "<b><c=ff0000>"
      msg_markup_end = "</c></b>"
    when "whisper"
      msg_markup_begin = "<i><c=ce54de>"
      msg_markup_end = "</c></i>"
    when "thought"
      msg_markup_begin = "<i><c=00ccff>"
      msg_markup_end = "</c></i>"
    when "system"
      msg_markup_begin = "<b><c=ffd736>"
      msg_markup_end = "</c></b>"
    else
      msg_markup_begin = ""
      msg_markup_end = ""
    end
    if @direction == :upward
      @history_text += "[#{from}]: " +
        msg_markup_begin + chat_message["message"] + msg_markup_end + "\n"
    elsif @direction == :downward
      @history_text = "[#{from}]: " +
        msg_markup_begin + chat_message["message"] +
        msg_markup_end + "\n" + @history_text
    else
      puts "ERROR: bad direction set in #chat"
      return

    end
    @history.drawable = Gosu::Image.from_text(@history_text, @font_size,
      width: @history.width)
  end
end
