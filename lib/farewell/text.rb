# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# @deprecated Use +Gosu::Image#from_text+ or +Gosu::Font+ instead.
# TODO: remove the * 2 in the @width setting in intitialize (it was there
# because the size of the fonts in tileset are twice larger than in the yaml)
# Warning: space char is checked in yaml rather than here
class Text < Gosu::Image
  TEXT_INFO = YAML.load_file('data/images/font_tileset2.yaml')

  attr_reader :width, :height
  def initialize(string, x: 0, y: 0, z: 0)
    @image_list = []
    @width_list = []
    i = 0 # Index of the following loop
    @height = 18
    @width = 0
    string.each_char do |char|
      @image_list.push(Window.instance.tileset_list[:font2][TEXT_INFO[char][0]])
      @width_list.push(TEXT_INFO[char][1])
      @width += (TEXT_INFO[char][1] + 1) * 2
      i += 1
    end
    @x = x
    @y = y
    @z = z
  end

  def draw(x, y, z, scale_x: 1, scale_y: 1, color: Gosu::Color::WHITE, mode: :default)
    i = 0 # Index of the following loop
    total_width = 0
    @image_list.each do |image|
      image.draw(x + @x + total_width * 2, y + @y, z + @z, scale_x, scale_y, color, mode)
      total_width += @width_list[i] + 1 unless @width_list[i].nil?
      i += 1
    end
  end
end
