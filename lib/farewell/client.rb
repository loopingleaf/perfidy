# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

module Farewell
  class Client
    attr_accessor :msg, :request_threads, :server, :response_messages

    def initialize
      @msg = nil
      @request_threads = []
      @response_thread = nil
      @response_messages = Queue.new
      @pending_messages = []
    end

    def connect
      @server = TCPSocket.new(Config::HOST, Config::PORT)
      listen
      true
    rescue Errno::ECONNREFUSED
      puts "Could not connect to the server"
      false
    end

    def listen
      @response_thread = Thread.new {
        loop do
          msg = @server.gets("\0").chomp
          msg = msg.delete("\000")
          @response_messages.push(JSON.parse(msg))
          puts "-> Client received message :"
          p JSON.parse(msg)
        end
      }
    end

    # Send a message to the server
    # @param msg [String] The message to send, usually in Json format.
    def send(msg)
      @request_threads.push(
        Thread.new {
          @server.send(msg + "\000", 0)
        }
      )
    end

    # Called in the main loop of the game to clean the queue, store the server's
    # messages and execute what they're meant to do.
    def update
      return if @response_messages.empty?

      msg = @response_messages.shift
      @pending_messages.push(msg)
    end

    # Return the coresponding header's pending message and delete it
    def get_pending(header_keyword)
      return if @pending_messages.empty?

      @pending_messages.each do |msg|
        key, body = msg.first
        if key == header_keyword
          @pending_messages.delete(msg)
          return body
        end
      end
      nil
    end

    def encode(message)
      message.unpack("c*").to_s
    end

    def decode(message)
      message.slice!(0)
      message.split(", ").map(&:to_i).pack("c*")
    end
  end
end
