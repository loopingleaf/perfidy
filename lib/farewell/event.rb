# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

module Farewell
  # An Event is blocks of code called whenever a trigger is pushed. Many
  # entities behave the same way, like buttons, collectibles, targets, etc.
  # Creating a button event for every button entities is an easy way to get
  # things done.
  #
  # @example
  #   event = Farewell::Event.new
  #   event.on_hover { |entity| entity.draw(color: Gosu::Color::AQUA) }
  #   @entity = Farewell::Entity.new(Gosu::Image.from_text("Blue if hovered"),
  #     0, 0, 0, h_align: :center)
  #   @entity.add_event(event)
  #
  # @todo
  #   on_click_down, on_right_click_down, on_hover_exit, on_hover_enter
  #   on_hover_stay, on_click_up, on_click_hold, on_right_click_up,
  #   on_right_click_hold... The "on_stay" methods should keep a backup entity
  #   to change the entity once the trigger is no more met.
  class Event
    # The trigger blocks and their triggers.
    # @return [Array<Array<Symbol, Proc>>] The custom blocks and their triggers
    attr_reader :on_trigger_blocks
    # The different custom triggers in the event. +:hover+ and +:click+ are
    # bluitin triggers, they don't belongs here and any attempt to add them in
    # this variable will lead to unexpected behaviors.
    # @return [Array<Symbol>] The list of custom triggers
    attr_reader :triggers

    def initialize
      @on_trigger_blocks = []
      @triggers = []
    end

    # +#on_hover+ events will be called when the attached entities are hovered by
    # the mouse
    # @param block The block to call once the click (mouse left) trigger is
    #   reached
    def on_hover(&block)
      @on_hover_block = block
    end

    # +#on_click+ events will be called when the attached entities are clicked
    # @param block The block to call once the click (mouse left) trigger is
    #   reached
    def on_click(&block)
      @on_click_block = block
    end

    # To call an event with a custom trigger (i.e. not on click, on hover, etc.)
    # @param trigger [Symbol] The name of the trigger. This same name will
    #   later be used with {#trigger}, or {Farewell::Entity#trigger_events}
    # @param block The block to call once the trigger is reached
    def on_trigger(trigger, &block)
      @on_trigger_blocks << [trigger, block]
      @triggers << trigger unless @triggers.include?(trigger)
    end

    # Will call every block corresponding to the trigger in the block lists.
    # @param trigger_event [Symbol] The trigger to call. `:hover` and `:click`
    #   are built-in triggers, they can be called even if the entity is not
    #   clicked or hovered
    # @param entity [Farewell::Entity] The entity to call with the block. As
    #   #trigger should be called by the lib or via
    #   {Farewell::Entity#trigger_events}, it correspond to one of the entities
    #   in which the event is attached, most of the time.
    def trigger(trigger_event, entity)
      if trigger_event == :hover
        return false if @on_hover_block.nil?

        @on_hover_block.call(entity)
        return true

      elsif trigger_event == :click
        return false if @on_click_block.nil?

        @on_click_block.call(entity)
        return true

      elsif @triggers.include?(trigger_event)
        count = 0
        @on_trigger_blocks.each do |tuple|
          if tuple[0] == trigger_event && !tuple[1].nil?
            tuple[1].call(entity)
            count += 1
          end
        end
        return count

      end
      false
    end
  end
end
