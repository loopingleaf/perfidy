# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

module Farewell
  # deprecated, now use Gosu::Image as drawable
  class Drawable
    attr_reader :width, :height
    def initialize(image, x: 0, y: 0, z: 0)
      if image.is_a?(Gosu::Image)
        @image = image
        @width = image.width
        @height = image.height
      end
      @x = x if x.is_a?(Integer)
      @y = y if y.is_a?(Integer)
      @z = z if z.is_a?(Integer)
    end

    def draw(x, y, z,
             scale_x: 1, scale_y: 1, color: 0xff_ffffff, mode: :default)
      @image.draw(x + @x, y + @y, z + @z, scale_x, scale_y, color, mode)
    end
  end
end
