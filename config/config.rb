# frozen_string_literal: true

module Config
  MAJOR         = 0
  MINOR         = 1
  PATCH         = 0
  VERSION       = '0.1.0'
  HOST          = '127.0.0.1'
  PORT          = 2000
  NATIVE_CURSOR = false
  LOCALE        = :fr
end
