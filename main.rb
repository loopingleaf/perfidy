#!/bin/ruby
# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# gems/modules
require "gosu"
require "socket"
require "yaml"
require "json"
require "singleton"
require "sqlite3"
require "timeout"
require "i18n"

# libs
require_relative "lib/farewell/main"

# config
require_relative "config/config"

# src
require_relative "src/super_entities/chat"
require_relative "src/salon"
require_relative "src/session"
require_relative "src/user"
require_relative "src/character"

# states
require_relative "src/states/connection_state"
require_relative "src/states/home_state"
require_relative "src/states/loading_state"
require_relative "src/states/play_state"
require_relative "src/states/preparation_state"
require_relative "src/states/settings_state"
require_relative "src/states/start_state"

# =============================================================================
# TODO: DELETE THIS LINE ->
$DEBUG = true
# =============================================================================

# i18n (internationalization)
I18n.load_path << Dir[File.expand_path("config/locales") + "/*.yml"]
I18n.default_locale = :fr
I18n.available_locales = [:fr, :en]
I18n.locale = Config::LOCALE

def t(*args)
  I18n.t(*args)
end

def l(*args)
  I18n.l(*args)
end

module Global
  CLIENT = Farewell::Client.new
  DATABASE = "data/#{I18n.locale}.db"
end

module Color
  PRIMARY = 0xff_f04005
  SECONDARY = 0xff_f7f7f7
end

class Window
  def close
    unless Session.instance.user.nil?
      Global::CLIENT.send(
        {disconnection: {token: Session.instance.user.token}}.to_json
      )
    end
    # TODO: wait a bit or wait for server to answer
    puts "Closing"
    Session.instance.close
    super
  end
end

puts "Starting"
Farewell::State.switch(StartState.instance)
Window.instance.show
