/* Characters
 * ================ */
INSERT INTO character VALUES(1, 'L''Immortel', 'Imm', 'N', 6);
INSERT INTO character VALUES(2, 'Elivia Vonsdav', 'Elivia', 'F', 4);
INSERT INTO character VALUES(3, 'Ludwig Jonshtaff', 'Ludwig', 'M', 4);
INSERT INTO character VALUES(4, 'Maevel Darma', 'Maeve', 'F', 2);
INSERT INTO character VALUES(5, 'Ev Owlinks', 'Ev', 'F', 3);
INSERT INTO character VALUES(6, 'Iridis Eliod', 'Eliod', 'M', 3);
INSERT INTO character VALUES(7, 'Skeel', 'Skeel', 'N', 2);
INSERT INTO character VALUES(8, 'Joseph Maxwell', 'Maxwell', 'M', 4);
INSERT INTO character VALUES(9, 'Étrange voyageur', 'Jill', 'N', 3);
INSERT INTO character VALUES(10, 'Karl Vank Moril', 'Karl', 'M', 4);
/* INSERT INTO character VALUES(11, 'Charlie Tremblin', 'Charlie', 'F', 4) */

/* Jobs
 * ================ */
INSERT INTO job VALUES(1, 'Immortelle', 'Immortel',
    'Les Immortels doivent leur nom du "ver immortel" qui les parasite,' ||
    ' duquel ils tirent leur incroyable longévité. Rare sont les immortels' ||
    ' qui, comme celui présent à la taverne, conservent leurs facultés' ||
    ' mentales. Il est cependant difficile de déterminer si ils sont' ||
    ' toujours animés par leur conscience, ou par celle du ver.');
INSERT INTO job VALUES(2, 'Chasseuse de primes', 'Chasseur de primes',
    'Il n''y a pas sot métier, et quand le crime paie aussi bien qu''en ces' ||
    ' temps, les chasseurs de primes font du zèle. Sa présence dans un lieu' ||
    ' si reculé dévoile ses funestes intentions : il est en chasse.');
INSERT INTO job VALUES(3, 'Répurgatrice', 'Répurgateur',
    'Personne ne vous dira qu''il souhaite être confronté au Tisse-brume,' ||
    ' mais aucun répurgateur ne vous avouera qu''il ne le souhaite pas.' ||
    ' Cette confrontation ornera fièrement son tableau de chasse, quelle' ||
    ' qu''en soit l''issue.');
INSERT INTO job VALUES(4, 'Alchimiste', 'Alchimiste',
    'La soif de savoir des Alchimistes est telle qu''elle va parfois à' ||
    ' l''encontre de la raison. Si vous ne pensez pas pouvoir faire' ||
    ' confiance à un alchimiste, rappellez-vous qu''il agira au nom de la' ||
    ' science, du moins avant que l''instinct de survie ne prenne le pas.');
INSERT INTO job VALUES(5, 'Docteure', 'Docteur',
    'Les Docteurs affiliés à l''Ordre des Médecins suivent une discipline' ||
    ' stricte, contraints par exemple de porter un masque blanc.' ||
    ' Mais les serments faiblissent à mesure que l''anxiété s''installe,' ||
    ' et les masques finissent toujours par tomber...');
INSERT INTO job VALUES(6, 'Marchande', 'Marchand',
    'Se prétendre Marchand sur ce continent vous fait passer soit pour un ' ||
    ' idiot, soit pour un opportuniste vraisamblablement malhonnête, car les' ||
    ' seuls marchés véritablement florissants ici sont le pillage et le larcin.');
INSERT INTO job VALUES(7, 'Princesse', 'Prince',
    'Ces nouvelles terres sont corrompues, inhospitalières et sans intérêt.' ||
    ' Aucun Monarque n''en voudrait. Sa présence ici soulève des question.');
INSERT INTO job VALUES(8, 'Compagne', 'Compagnon',
    'Voyager seul est difficile, surtout quand le sort nous enferme avec' ||
    ' de nombreux inconnus. Et quand il s''agit de survie, avoir quelqu''un' ||
    ' sur qui compter est loin d''être négligeable.');
INSERT INTO job VALUES(9, 'Assassin', 'Assassin',
    'Les Assassins se différencient des chasseurs de prime par le fait' ||
    ' qu''ils agissent pour le compte d''un ordre, d''une secte ou d''un' ||
    ' pays. Leurs cibles sont plus politiques que des voleurs ou des' ||
    ' criminels de bas étage.');
INSERT INTO job VALUES(10, 'Voleuse', 'Voleur',
    'On gagne sa vie comme on peut, et dans un pays où la loi du plus fort' ||
    ' s''applique, les Voleurs sont rois.');
INSERT INTO job VALUES(11, 'Paladin', 'Paladin',
    'Les Paladins sont des gardiens opérants pour le groupe auquel ils ont' ||
    ' prété allégeance. Ils peuvent endosser le rôle de hérauts, récruteurs ' ||
    ' ou agents de terrain, agissants en suivant les préceptes de leur groupe.');

/* Goals
 * ================ */
INSERT INTO goal VALUES(1, 'Soif de sang',
    'Chaque blessure grave infligée par vos soins vous donne satisfaction,' || 
        ' peu importe la cible (excepté vous).');
INSERT INTO goal VALUES(2, 'Expérimentations',
    'Vous commencez avec des échantillons étranges. Leurs effets sont' ||
        ' inconnus. Chaque ingestion par un autre survivant vous donne' ||
        ' satisfaction.');
INSERT INTO goal VALUES(3, 'Masochisme',
    'Chaque blessure que vous vous infligez vous donne satisfaction. Vous' ||
        ' avez accès à l''action "Automutilation" pour vous aider.');
INSERT INTO goal VALUES(4, 'Alcoolisme',
    'Boire de l''alcool vous donne satisfaction. Attention cependant,' ||
        ' épuiser les réserves vous déprime.');
/* TODO:
INSERT INTO goal VALUES(5, 'Pyromanie', );
INSERT INTO goal VALUES(6, 'Gardien de loi', );
INSERT INTO goal VALUES(7, 'Cleptomanie', );
INSERT INTO goal VALUES(8, 'Le travail avant tout', );
INSERT INTO goal VALUES(9, 'Claustrophobie');
INSERT INTO goal VALUES(10, 'Amour inavouable', );
*/
