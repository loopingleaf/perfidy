/* Jobs practiced by characters
 * ================ */
INSERT INTO practice VALUES(1, 1);
INSERT INTO practice VALUES(2, 2);
INSERT INTO practice VALUES(2, 3);
INSERT INTO practice VALUES(3, 2);
INSERT INTO practice VALUES(3, 3);
INSERT INTO practice VALUES(4, 4);
INSERT INTO practice VALUES(4, 5);
INSERT INTO practice VALUES(4, 6);
INSERT INTO practice VALUES(5, 2);
INSERT INTO practice VALUES(5, 4);
INSERT INTO practice VALUES(5, 7);
INSERT INTO practice VALUES(6, 2);
INSERT INTO practice VALUES(6, 8);
INSERT INTO practice VALUES(7, 2);
INSERT INTO practice VALUES(7, 9);
INSERT INTO practice VALUES(7, 10);
INSERT INTO practice VALUES(8, 2);
INSERT INTO practice VALUES(8, 11);
INSERT INTO practice VALUES(9, 2);
INSERT INTO practice VALUES(9, 9);
INSERT INTO practice VALUES(9, 10);
INSERT INTO practice VALUES(10, 2);
INSERT INTO practice VALUES(10, 6);
INSERT INTO practice VALUES(10, 11);

/* Job goals
 * ================ */
INSERT INTO job_goals VALUES(1, 6);
/*INSERT INTO job_goals VALUES(2, 8);*/
INSERT INTO job_goals VALUES(3, 1);
INSERT INTO job_goals VALUES(3, 3);
INSERT INTO job_goals VALUES(4, 2);
/*INSERT INTO job_goals VALUES(4, 5);*/
INSERT INTO job_goals VALUES(5, 1);
INSERT INTO job_goals VALUES(5, 2);
INSERT INTO job_goals VALUES(6, 4);
/*INSERT INTO job_goals VALUES(6, 9);*/
/*INSERT INTO job_goals VALUES(7, 7);*/
/*INSERT INTO job_goals VALUES(7, 9);*/
/*INSERT INTO job_goals VALUES(8, 10);*/
INSERT INTO job_goals VALUES(9, 2);
/*INSERT INTO job_goals VALUES(10, 7);*/
INSERT INTO job_goals VALUES(11, 4);
/*INSERT INTO job_goals VALUES(11, 6);*/
