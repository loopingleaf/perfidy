CREATE TABLE character(
    id INTEGER PRIMARY KEY
    ,name TEXT NOT NULL
    ,nickname TEXT NOT NULL
    ,gender TEXT DEFAULT 'N' CHECK(gender IN ('F', 'M', 'N'))
    ,life_count INTEGER DEFAULT 0
);

CREATE TABLE job(
    id INTEGER PRIMARY KEY
    ,f_name TEXT
    ,m_name TEXT
    ,description TEXT
);

CREATE TABLE practice(
    character_id INTEGER NOT NULL REFERENCES character(id)
    ,job_id INTEGER NOT NULL REFERENCES job(id)
    ,PRIMARY KEY(character_id, job_id)
);

CREATE TABLE goal(
    id INTEGER PRIMARY KEY
    ,name TEXT
    ,description TEXT
);

CREATE TABLE job_goals(
    job_id INTEGER NOT NULL REFERENCES job(id)
    ,goal_id INTEGER NOT NULL REFERENCES goal(id)
    ,PRIMARY KEY(job_id, goal_id)
);
