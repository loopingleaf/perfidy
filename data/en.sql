/* Characters
 * ================ */
INSERT INTO character VALUES(1, 'The Immortal', 'Imm', 'N', 9);
INSERT INTO character VALUES(2, 'Elivia Vonsdav', 'Elivia', 'F', 6);
INSERT INTO character VALUES(3, 'Ludwig Jonshtaff', 'Ludwig', 'M', 6);
INSERT INTO character VALUES(4, 'Maevel Darma', 'Maeve', 'F', 4);
INSERT INTO character VALUES(5, 'Ev Owlinks', 'Ev', 'F', 5);
INSERT INTO character VALUES(6, 'Iridis Eliod', 'Eliod', 'M', 5);
INSERT INTO character VALUES(7, 'Skeel', 'Skeel', 'N', 4);
INSERT INTO character VALUES(8, 'Joseph Maxwell', 'Maxwell', 'M', 6);
INSERT INTO character VALUES(9, 'Strange Traveler', 'Jill', 'N', 5); /* Their real name is actually Gilhelm Strange */
INSERT INTO character VALUES(10, 'Karl Vank Moril', 'Karl', 'M', 6);

/* Jobs
 * ================ */
INSERT INTO job VALUES(1, 'Immortal', 'Immortal');
INSERT INTO job VALUES(2, 'Bounty huntress', 'Bounty hunter');
INSERT INTO job VALUES(3, 'Witch huntress', 'Witch hunter');
INSERT INTO job VALUES(4, 'Alchemist', 'Alchemist');
INSERT INTO job VALUES(5, 'Doctor', 'Doctor');
INSERT INTO job VALUES(6, 'Merchant', 'Merchant');
INSERT INTO job VALUES(7, 'Princess', 'Prince');
INSERT INTO job VALUES(8, 'Companion', 'Companion');
INSERT INTO job VALUES(9, 'Assassin', 'Assassin');
INSERT INTO job VALUES(10, 'Thief', 'Thief');
INSERT INTO job VALUES(11, 'Paladin', 'Paladin');

/* Goals
 * ================ */
INSERT INTO goal VALUES(1, 'Bloodlust',
    'Each serious injury made by your hands grants you satisfaction, no' ||
        ' matter the target (except yourself).');
INSERT INTO goal VALUES(2, 'Experiments',
    'You begin with some strange samples. Their effects are unknown. Each' ||
        ' drink by another survivor grants you satisfaction.');
INSERT INTO goal VALUES(3, 'Masochism',
    'Each injury inflicted to yourself grants you satisfaction. You have' ||
        ' access to the action "Self-mutilation" to help you to do so.');
INSERT INTO goal VALUES(4, 'Alcoholism',
    'Drinking alcohol grants you satisfaction. Carefull though, depleted' ||
        ' stocks depress you.');
/* TODO:
INSERT INTO goal VALUES(5, 'Pyromania', );
INSERT INTO goal VALUES(6, 'Lawkeeper', );
INSERT INTO goal VALUES(7, 'Kleptomania', );
INSERT INTO goal VALUES(8, 'Job first', );
INSERT INTO goal VALUES(9, 'Claustrophobia', );
INSERT INTO goal VALUES(10, 'Hidden love', );
*/
